﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharpUnitTest.Tests
{
    [TestClass]
    public class EmployeeTest
    {
        [TestMethod]
        public void TestNullOrEmpty()
        {
            bool _isNull;
            Employee emp = new Employee();
            _isNull = emp.NullOrEmpty("Bryan");
            Assert.AreEqual(false, _isNull);


            _isNull = emp.NullOrEmpty("");
            Assert.AreEqual(true, _isNull);
        }
    }
}
